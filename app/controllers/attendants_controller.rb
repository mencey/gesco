# -*- encoding : utf-8 -*-
class AttendantsController < ApplicationController

  def search
    unless current_user.nil?
      @search = params[:id];
      @attendants = Attendant.find(:all, :conditions => {:meeting_id => params[:id]})
      render :index
    else
      redirect_to_login
    end
  end


  # GET /attendants
  # GET /attendants.xml
  def index
    unless current_user.nil?
      @attendants = Attendant.all
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @attendants }
      end
    else
      redirect_to_login
    end
  end

  # GET /attendants/1
  # GET /attendants/1.xml
  def show
    unless current_user.nil?
      @attendant = Attendant.find(params[:id])
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @attendant }
      end
    else
      redirect_to_login
    end
  end

  # GET /attendants/new
  # GET /attendants/new.xml
  def new
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @attendant = Attendant.new
        respond_to do |format|
          format.html # new.html.erb
          format.xml  { render :xml => @attendant }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # GET /attendants/1/edit
  def edit
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @attendant = Attendant.find(params[:id])
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # POST /attendants
  # POST /attendants.xml
  def create
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @attendant = Attendant.new(params[:attendant])
        respond_to do |format|
          if @attendant.save
            format.html { redirect_to(attendants_path + "/search/" + @attendant.meeting.id.to_s, :notice => 'Asistente creado satisfactoriamente.') }
            format.xml  { render :xml => @attendant, :status => :created, :location => @attendant }
          else
            format.html { render :action => "new" }
            format.xml  { render :xml => @attendant.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # PUT /attendants/1
  # PUT /attendants/1.xml
  def update
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @attendant = Attendant.find(params[:id])
        respond_to do |format|
          if @attendant.update_attributes(params[:attendant])
            format.html { redirect_to(attendants_path + "/search/" + @attendant.meeting.id.to_s, :notice => 'Asistente editado satisfactoriamente.') }
            format.xml  { head :ok }
          else
            format.html { render :action => "edit" }
            format.xml  { render :xml => @attendant.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # DELETE /attendants/1
  # DELETE /attendants/1.xml
  def destroy
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @attendant = Attendant.find(params[:id])
        @attendant.destroy
        respond_to do |format|
          format.html { redirect_to(:back) }
          format.xml  { head :ok }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end
end
