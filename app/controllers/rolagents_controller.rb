# -*- encoding : utf-8 -*-
class RolagentsController < ApplicationController

  def search
    unless current_user.nil?
      if is_admin || is_admin_UO || (current_user.id.to_s == params[:id])
        @search = params[:id];
        @rolagents = Rolagent.find(:all, :conditions => {:agent_id => params[:id]})
        render :index
      else
        render_500
      end
    else
      redirect_to_login
    end
  end

  # GET /rolagents
  # GET /rolagents.xml
  def index
    if is_admin || is_admin_UO
      @rolagents = Rolagent.all
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @rolagents }
      end
    else
      @rolagents = Rolagent.find(:all, :conditions => {:agent_id => current_user.id})
    end
  end
  
  def searchbycomm
     unless current_user.nil?
        @searchbycomm = params[:id]
        @rolagents = Rolagent.find(:all, :conditions => {:commission_id => params[:id] })
        render :index
     else 
      redirect_to_login
     end
  end

  # GET /rolagents/1
  # GET /rolagents/1.xml
  def show
    @rolagent = Rolagent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @rolagent }
    end
  end

  # GET /rolagents/new
  # GET /rolagents/new.xml
  def new
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @rolagent = Rolagent.new
        respond_to do |format|
          format.html # new.html.erb
          format.xml  { render :xml => @rolagent }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # GET /rolagents/1/edit
  def edit
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @rolagent = Rolagent.find(params[:id])
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # POST /rolagents
  # POST /rolagents.xml
  def create
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @rolagent = Rolagent.new(params[:rolagent])
        respond_to do |format|
          if @rolagent.save
            format.html { redirect_to("/rolagents/searchbycomm/" + @rolagent.commission.id.to_s, :notice => 'Rol creado satisfactoriamente.') }
            format.xml  { render :xml => @rolagent, :status => :created, :location => @rolagent }
          else
            format.html { render :action => "new" }
            format.xml  { render :xml => @rolagent.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end

  end

  # PUT /rolagents/1
  # PUT /rolagents/1.xml
  def update
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @rolagent = Rolagent.find(params[:id])
        respond_to do |format|
          if @rolagent.update_attributes(params[:rolagent])
            format.html { redirect_to("/rolagents/searchbycomm/" + @rolagent.commission.id.to_s, :notice => 'Rol editado satisfactoriamente.') }
            format.xml  { head :ok }
          else
            format.html { render :action => "edit" }
            format.xml  { render :xml => @rolagent.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # DELETE /rolagents/1
  # DELETE /rolagents/1.xml
  def destroy
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @rolagent = Rolagent.find(params[:id])
        @rolagent.destroy
        respond_to do |format|
          format.html { redirect_to(:back) }
          format.xml  { head :ok }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end
end
