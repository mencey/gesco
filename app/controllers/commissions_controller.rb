# -*- encoding : utf-8 -*-
class CommissionsController < ApplicationController


  # GET /commissions
  # GET /commissions.xml
  def index
  	 unless current_user.nil?
	  	 if is_admin || is_admin_UO
  		  	@commissions = Commission.all
		 else 
		 	@roles = Rolagent.find(:all, :conditions => {:agent_id => current_user.id}).map { |r| [r.commission.nombre]}
		 	@units = Rolagent.find(:all, :conditions => {:agent_id => current_user.id}).map { |r| [r.unit.id]}
  	  		@commissions = Commission.find(:all, :conditions => {nombre =>  @roles, :unidad_id => @units})

            respond_to do |format|
              format.html # index.html.erb
              format.xml  { render :xml => @commissions }
            end
         end
     else 
      redirect_to_login
    end
  end
  
  def searchbyunit
     unless current_user.nil?
        if is_admin || is_admin_UO
            @commissions = Commission.find(:all, :conditions => [ 'id != 0 AND unidad_id = ?', params[:id] ]) 
        else 
            @roles = Rolagent.find(:all, :conditions => {:agent_id => current_user.id}).map { |r| [r.commission.nombre]}
            @units = Rolagent.find(:all, :conditions => {:agent_id => current_user.id}).map { |r| [r.unit.id]}
            @id = Commission.find(:all, :conditions => [ 'id != 0 AND unidad_id = ?', params[:id] ]).map { |r| [r.id]}
            @commissions = Commission.find(:all, :conditions => {:id => @id, :nombre =>  @roles, :unidad_id => @units})
        end
        render :index
     else 
      redirect_to_login
     end
  end
    


  # GET /pdf/:id/:agent.pdf
  def pdf
	 unless params[:id].nil? and params[:a].nil? then
	 	 @rol = Rolagent.find(:all, :conditions => {:agent_id => params[:a], :commission_id => params[:id]}).map { |r| [r.commission_id] }
		 unless @rol.nil? or @rol.empty? then
		    @commission = Commission.find(:first, :conditions => {:id => @rol})

			 @valor = Value.find(:first, :conditions => {:codigo =>  "6.3"})
			 @secretario = Rolagent.find(:first, :conditions => {:commission_id => params[:id], :rol_id => @valor.id})

			 @agent = Agent.find(:first, :conditions => {:id => params[:a]})

			 @periodo = Rolagent.find(:first, :conditions => {:commission_id => params[:id], :agent_id => params[:a]})

			 @totales = Meeting.find(:all, :conditions => {:commission_id => params[:id]}).size
			 @reuniones = Meeting.find(:all, :conditions => {:commission_id => params[:id]}).map { |r| [r.id]}
			 @no = Value.find(:first, :conditions => {:codigo =>  "14.3"})
			 @asiste = Attendant.find(:all, :conditions => {:agent_id => params[:a], :meeting_id => @reuniones, :ausencia_id => @no.id}).size

			 render :show
		 else 
		 	redirect_to_login
		 end
	  end
  end

  # GET /commissions/1
  # GET /commissions/1.xml
  def show
    @commission = Commission.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
		format.pdf {render :layout => false}
     format.xml  { render :xml => @commission }
    end
  end

  # GET /commissions/new
  # GET /commissions/new.xml
  def new
    unless current_user.nil?
      if is_admin_UO || is_admin
        @commission = Commission.new
  
        respond_to do |format|
          format.html # new.html.erb
          format.xml  { render :xml => @commission }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # GET /commissions/1/edit
  def edit
    unless current_user.nil?
      if is_admin_UO || is_admin
        @commission = Commission.find(params[:id])
        #@meetings = Meeting.find_by_commission_id(@commission.id)
        #@roles = Rolagent.find_by_commission_id(@commission.id)
        #@agents = Agent.find_by_id(@roles.agent)
        #@docs = Doc.find_by_commission_id(@commission.id)
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # POST /commissions
  # POST /commissions.xml
  def create
    unless current_user.nil?
      if is_admin_UO || is_admin
        @commission = Commission.new(params[:commission])
    
        respond_to do |format|
          if @commission.save
            format.html { redirect_to(commissions_path + "/searchbyunit/" + @commission.unidad.id.to_s, :notice => 'Comisión creada satisfactoriamente.') }
            format.xml  { render :xml => @commission, :status => :created, :location => @commission }
          else
            format.html { render :action => "new" }
            format.xml  { render :xml => @commission.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # PUT /commissions/1
  # PUT /commissions/1.xml
  def update
    unless current_user.nil?
      if is_admin_UO || is_admin
        @commission = Commission.find(params[:id])
    
        respond_to do |format|
          if @commission.update_attributes(params[:commission])
            format.html { redirect_to(@commission, :notice => 'Comisión editada satisfactoriamente.') }
            format.xml  { head :ok }
          else
            format.html { render :action => "edit" }
            format.xml  { render :xml => @commission.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # DELETE /commissions/1
  # DELETE /commissions/1.xml
  def destroy
    unless current_user.nil?
      if is_admin_UO || is_admin
        @commission = Commission.find(params[:id])
        @commission.destroy
        respond_to do |format|
          format.html { redirect_to(:back) }
          format.xml  { head :ok }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end
end
