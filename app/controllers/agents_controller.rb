# -*- encoding : utf-8 -*-
class AgentsController < ApplicationController
  # GET /agents
  # GET /agents.xml
  def index
    unless current_user.nil?
      if is_admin || is_admin_UO || is_secre
        @agents = Agent.all
      else
        @agents = [Agent.find(current_user.id)]
      end
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @agents }
      end
    else
      redirect_to_login
    end
  end
  
  # GET /agents/1
  # GET /agents/1.xml
  def show 
    unless current_user.nil?
      if is_admin || is_admin_UO || (current_user.id.to_s == params[:id]) || is_secre
        @agent = Agent.find(params[:id])
        respond_to do |format|
          format.html # show.html.erb
  		  format.pdf {render :layout => false}
          format.xml  { render :xml => @agent }
        end
      else 
        render_500
      end        
    else
      redirect_to_login
    end     
  end

  # GET /agents/new
  # GET /agents/new.xml
  def new
    if is_admin || is_admin_UO || is_secre
      @agent = Agent.new
      respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @agent }
      end
    else
      render_500
    end
  end

  # GET /agents/1/edit
  def edit
    if is_admin || is_admin_UO || is_secre
      @agent = Agent.find(params[:id])
    else
      render_500
    end
  end

  # POST /agents
  # POST /agents.xml
  def create
    if is_admin || is_admin_UO || is_secre
      @agent = Agent.new(params[:agent])
  
      respond_to do |format|
        if @agent.save
          format.html { redirect_to(agents_path, :notice => 'Usuario creado satisfactoriamente.') }
          format.xml  { render :xml => @agent, :status => :created, :location => @agent }
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @agent.errors, :status => :unprocessable_entity }
        end
      end
    else
      render_500
    end
  end

  # PUT /agents/1
  # PUT /agents/1.xml
  def update
    if is_admin || is_admin_UO || is_secre
      @agent = Agent.find(params[:id])
  
      respond_to do |format|
        if @agent.update_attributes(params[:agent])
          format.html { redirect_to(agents_path, :notice => 'Usuario editado satisfactoriamente.') }
          format.xml  { head :ok }
        else
          format.html { render :action => "edit" }
          format.xml  { render :xml => @agent.errors, :status => :unprocessable_entity }
        end
      end
    else
      render_500
    end
  end

  # DELETE /agents/1
  # DELETE /agents/1.xml
  def destroy
    if is_admin || is_admin_UO || is_secre
      @agent = Agent.find(params[:id])
      @agent.destroy
  
      respond_to do |format|
        format.html { redirect_to(:back) }
        format.xml  { head :ok }
      end
    else
      render_500
    end
  end
end
