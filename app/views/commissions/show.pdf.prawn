pdf.image "#{RAILS_ROOT}/public/images/pdf_top.jpg"
pdf.image "#{RAILS_ROOT}/public/images/fachada.jpg"
pdf.move_up 500


unless @commission.nil?
   pdf.text @secretario.agent.nombre + " " + @secretario.agent.apellido1 + " " + @secretario.agent.apellido2 + ", Secretario de " + @commission.nombre + " de la Universidad de La Laguna."
   pdf.move_down 20
   pdf.text "CERTIFICA:"
   pdf.move_down 20
	unless @periodo.fechabaja.nil?
	   pdf.text "D./DÑA. " + @agent.nombre + " " + @agent.apellido1 + " " + @agent.apellido2 + " con " + @agent.tipodoc.description + " " + @agent.numdoc + ", ha sido miembro de la comisión " + @commission.nombre + " de la unidad organizativa " + @secretario.unit.nombre + " en los siguientes periodos: desde " + @periodo.fechaalta.strftime("%Y-%m-%d") + " hasta " + @periodo.fechabaja.strftime("%Y-%m-%d")
	else
		tiempo = Time.new
	   pdf.text "D./DÑA. " + @agent.nombre + " " + @agent.apellido1 + " " + @agent.apellido2 + " con " + @agent.tipodoc.description + " " + @agent.numdoc + ", ha sido miembro de la comisión " + @commission.nombre + " de la unidad organizativa " + @secretario.unit.nombre + " en los siguientes periodos: desde " + @periodo.fechaalta.strftime("%Y-%m-%d") + " hasta " + tiempo.strftime("%Y-%m-%d")
   end
   pdf.move_down 20
	pdf.text "Durante ese período se celebraron " +  @totales.to_s + " reuniones, "
	pdf.text "de las que asistió a " + @asiste.to_s + " lo que supone una proporción del " + (@asiste * 100 / @totales).to_s + "%"
   pdf.move_down 20
   pdf.text "Para que conste ante quien corresponda, firmo la presente en La Laguna a, " + tiempo.day.to_s + " de " + tiempo.strftime("%c") + " de " + tiempo.year.to_s + "."
end

