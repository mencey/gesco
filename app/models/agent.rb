# -*- encoding : utf-8 -*-
class Agent < ActiveRecord::Base
  belongs_to :tipo, :class_name => "Value"
  belongs_to :estado, :class_name => "Value"
  belongs_to :tipodoc, :class_name => "Value"
  belongs_to :tipoauth, :class_name => "Value"
  has_many :attendants
#  belongs_to :rolagent, :class_name => "rolagent"
  
  acts_as_authentic do |c|
    c.login_field=:usuario
    c.logged_in_timeout = 30.minutes
  end

  def dn
    "cn=#{self.email},ou=people,dc=mydomain"
  end

  def password_correcta?(password_plaintext)
#    ldap = LdapConnect.new.ldap
#       ldap.auth self.dn, password_plaintext
#        if (ldap.bind) # will return false if authentication is NOT successful
#          return true
#        end
    return valid_password?(password_plaintext)
  end
end
