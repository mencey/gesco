# -*- encoding : utf-8 -*-
class Attendant < ActiveRecord::Base
  belongs_to :meeting
  belongs_to :agent
  belongs_to :estado, :class_name => "Value"
  belongs_to :ausencia, :class_name => "Value"
end
