# -*- encoding : utf-8 -*-
class Value < ActiveRecord::Base
	has_many :agents
	has_many :commissions
	has_many :attendants
	has_many :rolagents
end
