# -*- encoding : utf-8 -*-
class Meeting < ActiveRecord::Base
  belongs_to :commission
  belongs_to :estado, :class_name => "Value"
  belongs_to :doc
  has_many :attendants
end
