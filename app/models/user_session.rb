# -*- encoding : utf-8 -*-
class UserSession < Authlogic::Session::Base
  def to_key
     new_record? ? nil : [ self.send(self.class.primary_key) ]
  end
  
  def persisted?
    false
  end
  authenticate_with Agent
  verify_password_method :password_correcta?
end


