# -*- encoding : utf-8 -*-
class AddVisibilidadToCommission < ActiveRecord::Migration
  def self.up
    add_column :commissions, :visibilidad, :integer
  end

  def self.down
    remove_column :commissions, :visibilidad
  end
end
