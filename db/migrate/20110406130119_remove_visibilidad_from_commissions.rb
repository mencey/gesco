# -*- encoding : utf-8 -*-
class RemoveVisibilidadFromCommissions < ActiveRecord::Migration
  def self.up
  	remove_column :commissions, :visibilidad
  end

  def self.down
  	add_column :commissions, :visibilidad, :integer
  end
end
