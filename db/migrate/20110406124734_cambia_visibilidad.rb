# -*- encoding : utf-8 -*-
class CambiaVisibilidad < ActiveRecord::Migration
  def self.up
    change_table :commissions do |t|
	 	t.references :visibilidad, :class_name => 'Value'
	 end
  end

  def self.down
    drop_column :commissions, :visibilidad
  end
end
