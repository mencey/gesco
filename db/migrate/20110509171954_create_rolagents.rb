# -*- encoding : utf-8 -*-
class CreateRolagents < ActiveRecord::Migration
  def self.up
    create_table :rolagents do |t|
      t.references :rol
      t.references :agent
      t.references :unit
      t.references :commission
      t.datetime :fechaalta
      t.datetime :fechabaja
      t.text :motivobaja
      t.references :estado

      t.timestamps
    end
  end

  def self.down
    drop_table :rolagents
  end
end
