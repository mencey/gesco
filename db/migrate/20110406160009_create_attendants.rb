# -*- encoding : utf-8 -*-
class CreateAttendants < ActiveRecord::Migration
  def self.up
    create_table :attendants do |t|
      t.references :meeting
      t.references :agent
      t.references :estado
      t.text :description
      t.references :ausencia

      t.timestamps
    end
  end

  def self.down
    drop_table :attendants
  end
end
