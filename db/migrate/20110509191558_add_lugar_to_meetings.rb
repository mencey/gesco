# -*- encoding : utf-8 -*-
class AddLugarToMeetings < ActiveRecord::Migration
  def self.up
    add_column :meetings, :lugar, :string
  end

  def self.down
    remove_column :meetings, :lugar
  end
end
