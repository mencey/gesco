# -*- encoding : utf-8 -*-
class CreateDocs < ActiveRecord::Migration
  def self.up
    create_table :docs do |t|
      t.references :commission
      t.string :url

      t.timestamps
    end
  end

  def self.down
    drop_table :docs
  end
end
