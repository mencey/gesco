# -*- encoding : utf-8 -*-
class CreateCommissions < ActiveRecord::Migration
  def self.up
    create_table :commissions do |t|
      t.references :tipo
      t.references :estado
      t.references :unidad
      t.string :nombre
      t.text :nombrelargo
      t.text :funciones
      t.datetime :fechacreacion
      t.datetime :fechabaja
      t.text :motivobaja
      t.text :composicion
      t.integer :visibilidad

      t.timestamps
    end
  end

  def self.down
    drop_table :commissions
  end
end
