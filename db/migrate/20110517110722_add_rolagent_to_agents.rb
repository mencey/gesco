# -*- encoding : utf-8 -*-
class AddRolagentToAgents < ActiveRecord::Migration
  def self.up
    add_column :agents, :rolagent, :string
  end

  def self.down
    remove_column :agents, :rolagent
  end
end
