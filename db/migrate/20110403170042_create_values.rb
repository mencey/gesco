# -*- encoding : utf-8 -*-
class CreateValues < ActiveRecord::Migration
  def self.up
    create_table :values do |t|
      t.string :codigo
      t.text :tabla
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :values
  end
end
